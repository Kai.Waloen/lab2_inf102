package INF102.lab2.runtime;

public class RuntimeAnswers {
/*
    public double intrestLoan(double amount, int n) { //O(n)
        for (int i = 0; i < n; i++) { //O(n)
            amount = amount * 1.01; //O(1)
        }
        return amount;
    }
*/

    public String taskA() {
        return "g";
    }

    /*
    public static int countOneBits(int n) { // O(n)
        int bits = 0;
        while (n > 0) { // O(n) iterations
            if (n % 2 == 0) { //O(1)
                bits++;
            }
            n = n / 2; //O(1)
        }
        return bits;
    }
*/

    public String taskB() {
        return "c";
    }

    /*
    public static int countSteps(int n) { // O(nlog(n))
        int pow = 2;
        int steps = 0;
        for (int i = 0; i < n; i++) { // O(n) iterations
            if (i == pow) {
                pow *= 2; // O(1)
                for (int j = 0; j < n; j++) { // every other iteration pow doubles therefore log(n)
                    steps++;
                }
            }
            else {
                steps++;
            }
        }
        return steps;
    }
*/

    public String taskC() {
        return "d";
    }

    /*
    public static String markRandomString(int n) { // O(n^2)
        String ans = "";
        for (int i = 0; i < n; i++) { //O(n) iterations
            char c = (char) ('a' + 26*Math.random()); //O(1)
            ans += c; // concatination step involves copying string to new string therefore O(n)
        }
        return ans;
    }
*/

    public String taskD() {
        return "b";
    }

}
