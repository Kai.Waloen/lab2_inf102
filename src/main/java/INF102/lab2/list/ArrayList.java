package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {


    public static final int DEFAULT_CAPACITY = 10;

    private int n;

    private Object elements[];

    public ArrayList() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index > size()})
     */

    @Override
    public T get(int index) {
        if (index < 0 || index > size()-1) {
            throw new IndexOutOfBoundsException();

        }
        return (T) elements[index];
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index >= size()})
     */

    //This method was built by help of copilot
    @Override
    public void add(int index, T element) {
        //throw exception if the index is out of range
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }

        //if the array is full, double the size
        if (n == elements.length) {
            elements = Arrays.copyOf(elements, elements.length * 2);
        }

        //if the array is empty, add the element
        if (n == 0) {
            elements[index] = element;
        }

        //shift the elements to the right
        for (int i = n; i > index; i--) {
            elements[i] = elements[i - 1];
        }

        //insert the element
        elements[index] = element;

        //increment the size
        n++;

    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(n * 3 + 2);
        str.append("[");
        for (int i = 0; i < n; i++) {
            str.append((T) elements[i]);
            if (i != n - 1) str.append(", ");
        }
        str.append("]");
        return str.toString();
    }



}