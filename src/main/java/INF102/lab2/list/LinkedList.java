package INF102.lab2.list;

public class LinkedList<T> implements List<T> {

	private int n;
	
	/**
	 * If list is empty, head == null
	 * else head is the first element of the list.
	 */
	private Node<T> head;

	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}

	@Override
	public T get(int index) {
		return getNode(index).data;
	}
	
	/**
     * Returns the node at the specified position in this list.
     *
     * @param index index of the node to return
     * @return the node at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */

	//Got help from copilot
	private Node<T> getNode(int index) {

		//throw out of bounds exception if index is out of range
		if (index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException();
		}

		//iterate through the nodes to find correct index
		Node<T> currentNode = head;
		for (int i = 0; i < index; i++) {
			currentNode = currentNode.next;
		}
		return currentNode;

	}

	//Built with the help of copilot
	@Override
	public void add(int index, T element) {

		//if index is not zero, make a new node and add the element
		if (index != 0) {
			Node<T> newNode = new Node<T>(element);
			Node<T> previousNode = getNode(index - 1);

			//link the previous node (index-1) to the new node
			newNode.next = previousNode.next;
			previousNode.next = newNode;

		//if index is zero, add the element to the head
		} else {
			Node<T> newNode = new Node<T>(element);

			//link the new node to the head
			newNode.next = head;

			//make the new node the head
			head = newNode;
		}

		//increment the size
		n++;
	}


	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		Node<T> currentNode = head;
		while (currentNode.next != null) {
			str.append(currentNode.data);
			str.append(", ");
			currentNode = currentNode.next;
		}
		str.append((T) currentNode.data);
		str.append("]");
		return str.toString();
	}

	@SuppressWarnings("hiding")
	private class Node<T> {
		T data;
		Node<T> next;

		public Node(T data) {
			this.data = data;
		}
	}
	
}